package apps

import (
	"gitee.com/code-horse-mi/mcube/logger"
	"github.com/gin-gonic/gin"
)

// 定义注册和初始化gin实例的接口
type HttpIocObject interface {
	IocObject
	RegistryHandler(gin.IRouter)
}

// 定义存放所有内部注册gin实例的map
var (
	httpAppStore = map[string]HttpIocObject{}
)

// 定义gin实例注册的方法
func RegistryHttpApp(obj HttpIocObject) {
	if _, ok := httpAppStore[obj.Name()]; !ok {
		httpAppStore[obj.Name()] = obj
		logger.L().Debug().Msgf(obj.Name() + " http实例注册成功!")
		return
	}
	logger.L().Panic().Msgf(obj.Name() + " http实例已经注册!")
}

// 定义获取注册gin实例的方法
func GetHttpApp(objName string) any {
	v, ok := httpAppStore[objName]
	if !ok {
		logger.L().Panic().Msgf(objName + " http实例不存在, 请首先注册!")
	}
	return v
}

// LoadedGinApp 查询加载成功的服务
func LoadedGinApp() (apps []string) {
	for k := range httpAppStore {
		apps = append(apps, k)
	}
	return
}
