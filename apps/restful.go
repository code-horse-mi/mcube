package apps

import (
	"gitee.com/code-horse-mi/mcube/logger"
	"github.com/emicklei/go-restful/v3"
)

// 存放restful接口的map
var (
	restfulApps = map[string]RESTfulApp{}
)

// HTTPService Http服务的实例
type RESTfulApp interface {
	IocObject
	Registry(*restful.WebService)
	Version() string
}

// RegistryRESTfulApp 服务实例注册
func RegistryRESTfulApp(app RESTfulApp) {
	// 已经注册的服务禁止再次注册
	_, ok := restfulApps[app.Name()]
	if ok {
		logger.L().Panic().Msgf(app.Name() + " resetful实例已经注册!")
	}
	restfulApps[app.Name()] = app
	logger.L().Debug().Msgf(app.Name() + " resetful实例注册成功!")
}

// LoadedHttpApp 查询加载成功的服务
func LoadedRESTfulApp() (apps []string) {
	for k := range restfulApps {
		apps = append(apps, k)
	}
	return
}

// 获取Restful App
func GetRESTfulApp(name string) RESTfulApp {
	app, ok := restfulApps[name]
	if !ok {
		logger.L().Panic().Msgf(name + " resetful实例不存在, 请首先注册!")
	}
	return app
}
