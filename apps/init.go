package apps

import (
	"fmt"
	"strings"

	"gitee.com/code-horse-mi/mcube/logger"
	"github.com/emicklei/go-restful/v3"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/restful/accessor/form"
	"github.com/infraboard/mcube/http/restful/accessor/yaml"
	"github.com/infraboard/mcube/http/restful/accessor/yamlk8s"
	"google.golang.org/grpc"
)

// 初始化配置内部实例
func InitInternalApps() error {
	for _, v := range internalAppStore {
		if err := v.Conf(); err != nil {
			return err
		}
		logger.L().Debug().Msgf(v.Name() + "实例初始化配置成功!")
	}
	return nil
}

// 初始化配置gin实例
func InitHttpApps(r gin.IRouter) error {
	for _, v := range httpAppStore {
		if err := v.Conf(); err != nil {
			return err
		}
		v.RegistryHandler(r.Group(v.Name()))
		logger.L().Debug().Msgf(v.Name() + " http实例初始化配置成功!")
	}
	return nil
}

// 初始化配置grpc实例
func InitGrpcApps(server *grpc.Server) error {
	for _, v := range grpcAppStore {
		if err := v.Conf(); err != nil {
			return err
		}
		v.RegistryHandler(server)
		logger.L().Debug().Msgf(v.Name() + " grpc实例初始化配置成功!")
	}
	return nil
}

// 初始化配置restful实例
func InitRestfulApps(pathPrefix string, root *restful.Container) error {
	for _, api := range restfulApps {
		if err := api.Conf(); err != nil {
			return err
		}
		pathPrefix = strings.TrimSuffix(pathPrefix, "/")
		ws := new(restful.WebService)
		ws.
			Path(fmt.Sprintf("%s/%s/%s", pathPrefix, api.Version(), api.Name())).
			Consumes(restful.MIME_JSON, restful.MIME_XML, form.MIME_POST_FORM, form.MIME_MULTIPART_FORM, yaml.MIME_YAML, yamlk8s.MIME_YAML).
			Produces(restful.MIME_JSON, restful.MIME_XML, yaml.MIME_YAML, yamlk8s.MIME_YAML)
		api.Registry(ws)
		root.Add(ws)
		logger.L().Debug().Msgf(api.Name() + " restful实例初始化配置成功!")
	}
	return nil
}
