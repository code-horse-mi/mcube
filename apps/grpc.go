package apps

import (
	"gitee.com/code-horse-mi/mcube/logger"
	"google.golang.org/grpc"
)

// 定义注册和初始化grpc实例的接口
type GrpcIocObject interface {
	IocObject
	RegistryHandler(*grpc.Server)
}

// 定义存放所有内部注册grpc实例的map
var (
	grpcAppStore = map[string]GrpcIocObject{}
)

// 定义grpc实例注册的方法
func RegistryGrpcApp(obj GrpcIocObject) {
	if _, ok := grpcAppStore[obj.Name()]; !ok {
		grpcAppStore[obj.Name()] = obj
		logger.L().Debug().Msgf(obj.Name() + " grpc实例注册成功!")
		return
	}
	logger.L().Panic().Msgf(obj.Name() + " grpc实例已经注册!")
}

// 定义获取注册grpc实例的方法
func GetGrpcApp(objName string) any {
	v, ok := grpcAppStore[objName]
	if !ok {
		logger.L().Panic().Msgf(objName + " grpc实例不存在, 请首先注册!")
	}
	return v
}

// LoadedGrpcApp 查询加载成功的服务
func LoadedGrpcApp() (apps []string) {
	for k := range grpcAppStore {
		apps = append(apps, k)
	}
	return
}
