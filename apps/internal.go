package apps

import "gitee.com/code-horse-mi/mcube/logger"

// 定义注册和初始化内部实例的接口
type IocObject interface {
	Name() string
	Conf() error
}

// 定义存放所有内部注册实例的map
var (
	internalAppStore = map[string]IocObject{}
)

// 定义内部实例注册的方法
func RegistryInternalApp(obj IocObject) {
	if _, ok := internalAppStore[obj.Name()]; !ok {
		internalAppStore[obj.Name()] = obj
		logger.L().Debug().Msgf(obj.Name() + "实例注册成功!")
		return
	}
	logger.L().Panic().Msgf(obj.Name() + "实例已经注册!")
}

// 定义获取注册实例的方法
func GetInternalApp(objName string) any {
	v, ok := internalAppStore[objName]
	if !ok {
		logger.L().Panic().Msgf(objName + "实例不存在, 请首先注册!")
	}
	return v
}

// LoadedInternalApp 查询加载成功的服务
func LoadedInternalApp() (apps []string) {
	for k := range internalAppStore {
		apps = append(apps, k)
	}
	return
}
