package page

const (
	// 设置默认分页大小
	DefaultPageSize = 20
	// 设置默认分页号
	DefaultPageNumber = 1
)

// 计算PageRequest中Offset的值
func (p *PageRequest) ComputeOffset() int64 {
	if p.Offset != 0 {
		return p.Offset
	}
	return int64(p.PageSize * (p.PageNumber - 1))
}

// PageRequest构造函数
func NewPageRequest(pagesize uint, pagenumber uint) *PageRequest {
	return &PageRequest{
		PageSize:   uint64(pagesize),
		PageNumber: uint64(pagenumber),
	}
}

// PageRequest默认构造函数
func NewDefaultPageRequest() *PageRequest {
	return NewPageRequest(DefaultPageSize, DefaultPageNumber)
}
